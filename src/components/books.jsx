import React, { Component } from "react";
import BooksTable from "./booksTable";
import ListGroup from "./common/listGroup";
import Pagination from "./common/pagination";
import { getBooks } from "../services/fakeBookService";
import { getGenres } from "../services/fakeGenreService";
import { paginate } from "../utils/paginate";
import CategorySelect from "./common/categorySelect";
import Input from "./common/input"
import _ from "lodash";


class Books extends Component {
  state = {
    books: [],
    genres: [],
    category: "All",
    selectedCategory: 'All',
    selectedAuthor: "",
    selectedBook: "",
    currentPage: 1,
    pageSize: 4,
    sortColumn: { path: "title", order: "asc" }
  };

  componentDidMount() {
    // this.handleChange = this.handleChange.bind(this);

    const genres = [{ _id: "", name: "All Genres" }, ...getGenres()];

    this.setState({ books: getBooks(), genres });
  }

  handleDelete = book => {
    const books = this.state.books.filter(m => m._id !== book._id);
    this.setState({ books });
  };

  handleLike = book => {
    const books = [...this.state.books];
    const index = books.indexOf(book);
    books[index] = { ...books[index] };
    books[index].liked = !books[index].liked;
    this.setState({ books });
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  /*handleGenreSelect = genre => {
    this.setState({ selectedGenre: genre, currentPage: 1 });
    console.log("genre  " + genre)
    console.log("genre select =" + genre);
  };*/

  handleChange(selectedOption) {
    console.log("handleChange selectedOption  " + selectedOption)
    console.log("handleChange category  =" + selectedOption.target.value)
    this.setState({ category: selectedOption.target.value });
  }
  handleAuthorChange(selectedAuthor) {
    console.log("handleChange selectedAuthor  " + selectedAuthor);
    console.log("handleChange selectedAuthor.target.value = " + selectedAuthor.target.value);
    this.setState({ selectedAuthor: selectedAuthor.target.value });
  }

  handleBookChange(selectedBook) {
    console.log("handleChange selectedBook  " + selectedBook)
    console.log("handleChange selectedBook  " + selectedBook.target.value)
    this.setState({ selectedBook: selectedBook.target.value });
  }

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  handleReset = (e) => {
    console.log("reached handleReset this=" + this.state);
    console.log("reached handleReset e=" + e);
    //this.setState({ sortColumn }
    //this.setState({ selectedValue:"All" });
    this.setState({ category: "All" });
    this.setState({ selectedBook: "" });
    this.setState({ selectedAuthor: "" });
    this.setState({ books: getBooks() });

  }

  handleSearch = (e) => {
    console.log("reached handleSearch this=" + this.state);
    //this.setState({ sortColumn }
    //var books = this.state.books;
    var books = getBooks();
    var category = this.state.category;
    var author = this.state.selectedAuthor;
    var book = this.state.selectedBook;
    console.log("category = " + category);
    console.log("author = " + author);
    console.log("book " + book);

    //const books = this.state.books.filter(m => m.book.genre == book.genre);
    //var newBooks = new Array();
    var newBooks = [];
    if (category !== "All") {
      for (var i = 0; i < books.length; i++) {

        if (books[i].genre.name === category) {
          newBooks.push(books[i]);
        }

      }
      books = newBooks;
    }
    
    newBooks = [];

    if (author.length > 0) {
      for (var i = 0; i < books.length; i++) {

        if (books[i].author == author) {
          newBooks.push(books[i]);
        }

      }
      books = newBooks;
    }

    newBooks = [];
    if (book.length > 0) {
      for (var i = 0; i < books.length; i++) {

        if (books[i].title === book) {
          newBooks.push(books[i]);
        }
      }
      books = newBooks;
    }
    //var  books = [...this.state.books];
    this.setState({ books: books });
    //this.setState({ books });
    //return false;

    console.log("this.selectedGenre.props.e" + this.props.e);


  };


  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      sortColumn,
      selectedGenre,
      books: allBooks
    } = this.state;

    const filtered =
      selectedGenre && selectedGenre._id
        ? allBooks.filter(m => m.genre._id === selectedGenre._id)
        : allBooks;

    const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    const books = paginate(sorted, currentPage, pageSize);

    return { totalCount: filtered.length, data: books };
  };

  render() {
    const { length: count } = this.state.books;
    const { pageSize, currentPage, sortColumn } = this.state;

    if (count === 0) return <p>There are no books in the database.</p>;

    const { totalCount, data: books } = this.getPagedData();

    return (
      <div>
        {/* <CategorySelect onChange={this.handleChange}/>
        <Select onChange={(e) =>this.handleChange(e)}/>*/ }
        <table>
          <tr>
            <td>Category</td>
            <td>
              <select onChange={(e) => this.handleChange(e)}
                value={this.state.category} >
                <option value="All">All</option>
                <option value="Action">Action</option>
                <option value="Comedy">Comedy</option>
                <option value="Thriller">Thriller</option>
              </select>
            </td>
          </tr>
          <tr>
          <td>Author</td>
            <td>
              <input
                type="text"
                name="author"
                label="Author"
                value={this.state.selectedAuthor}
                onChange={(e) => this.handleAuthorChange(e)}
              />
            </td>
            </tr>
            <tr>
            <td>Book</td>
            <td>
              <input
                type="text"
                name="book"
                label="Book Name"
                value={this.state.selectedBook}
                onChange={(e) => this.handleBookChange(e)}
              />
            </td>
          </tr>
          <tr>
          <td>
          <button
            className="btn btn-primary"
            onClick={this.handleSearch}
          >
            Search
          </button>
          </td>
          <td>
          <button
            className="btn btn-primary"
            onClick={this.handleReset}
          >
            Reset
          </button>
          </td>
          </tr>
        </table>
        <div>
          <p>Showing {totalCount} books in the database.</p>
          <BooksTable
            books={books}
            sortColumn={sortColumn}
            onLike={this.handleLike}
            onDelete={this.handleDelete}
            onSort={this.handleSort}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>

      </div>
    );
  }
}

export default Books;
