import React, { Component } from "react";

import Select from 'react-select';

const Category = [
    { label: "All", value: "All" },
    { label: "Action", value: "Action" },
    { label: "Comedy", value: "Comedy" },
    { label: "Thriller", value: "Thriller" },
  ];
  class CategorySelect extends Component {
    render() {
      return (
        
            <div>
              <Select options={Category} />
            </div>
          
         
      );
    }
  }
  export default CategorySelect