import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import globalVal from './globalVar';
import {
  SessionStorageProvider,
  useSessionStorage,
} from "react-sessionstorage";

class LoginForm extends Form {
  constructor(props) {
    super(props);
    this.state = {
      data: { username: "", password: "", isLoggedin: false },
      errors: {}
    };
    { sessionStorage.setItem('loginMessage', "Guest") }
    //this.handleEvent = this.handleEvent.bind(this);  
  }


  schema = {
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = () => {
    console.log("in doSubmit of loginForm");
    let user = this.state.data.username;
    let loginMessage = "Guest";
    if (!this.isLoggedin) {


      if (user.length > 0) {
        loginMessage = " Logged in as " + user;
        this.isLoggedin = true;
      }
      else {
        // loginMessage = " Guest ";

      }
      sessionStorage.setItem('loginMessage', loginMessage);
    }
  };

  render() {
    return (
      <div>
        <h1>Login</h1>
        <form onSubmit={this.doSubmit}  action="/books">
          <table>
          <tr>
            <td>Username</td>
            <td>
              <input
                type="text"
                name="username"
                label="Username"
                onChange={(e) => this.handleChange(e)}
              /> </td></tr>
          {/*this.renderInput("username", "Username")}
          {//this.renderInput("password", "Password", "password")*/}
          <tr>
            <td>Password</td>
            <td>
              <input
                type="password"
                name="password"
                label="Password"
                onChange={(e) => this.handleChange(e)}
              /> </td>
              </tr>
               <tr>
            <td>Password</td>
            <td>
              {this.renderButton("Login")}
            </td>
          </tr>
          </table>
        </form>
      </div>
    );
  }
}

export default LoginForm;
