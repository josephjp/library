import React from "react";
import { getBooks } from "../services/fakeBookService";
import { getBook } from "../services/fakeBookService";

import { getGenres } from "../services/fakeGenreService";

const BookForm = ({ match, history }) => {
  console.log(this.props);
  console.log(match);
  console.log(history);
  var myBook = getBook(match.params.id);
  console.log("myBook" + myBook);
  console.log("myBook genre" + myBook.genre);
  return (
    <div>
      <h1>Book Details 
      </h1>
      <table>
        <tr> 
           <td>Author</td> <td>  {myBook.author}</td>
        </tr>
        <tr>
          <td>Title:</td> <td>  {myBook.title}</td>
        </tr>
        {/*<tr>
          <td> Category</td> <td> {myBook.genre.name}</td>
  </tr>*/}
        <tr>
          <td>Publish Date </td> <td>  {myBook.publishDate}</td>
  </tr> 
      </table>
      <button
        className="btn btn-primary"
        onClick={() => history.push("/books")}
      >
        Save
      </button>
    </div>
  );
};

export default BookForm;
